class Student{
    String fullName;
    int rollNum;
    double semPerentage;
    String collegeName;
    int collegeCode;
    //constructor
    Student(String fn,int rn,double sp,String cn,int cc){
        fullName=fn;
        rollNum=rn;
        semPerentage=sp;
        collegeCode=cc;
        collegeName=cn;
    }
    void display(){
        System.out.println("fullName : "+fullName);
        System.out.println("rollNum : "+rollNum);
        System.out.println("semPerentage : "+semPerentage);
        System.out.println("collegeName : "+collegeName);
        System.out.println("collegeCode : "+collegeCode);
    }
    //destructor
    protected void finalize(){
        System.out.println("Dead");
    }
}
public class constructor1 {
    public static void main(String []args){
        Student s=new Student("Ravi",594,69,"mvgr",33);
        s.display();
        s=null;
        System.gc();
    }
}
