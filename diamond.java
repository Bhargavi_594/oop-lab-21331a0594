interface s1{
    public default void dis(){
        System.out.println("s1 interface");
    }
}
interface s2 extends s1{
    public default void dis1(){
        System.out.println("s2 interface");
    }
}
interface s3 extends s1{
    public default void dis2(){
        System.out.println("s3 interface");
    }
}
class s4 implements s2,s3{
    void display(){
        s2.super.dis1();
        s3.super.dis();
        s2.super.dis();
    }
}
public class diamond {
    public static void main(String []args){
        s4 s=new s4();
        s.display();
    }
}
