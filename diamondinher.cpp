#include<iostream>
using namespace std;
class parent{
    public:
    int n=5;
    void display(){
        cout<<"parent class"<<endl;
    }
};
class child1:public parent{
    public:
    float f=1.2;
    void disp(){
        cout<<"child class"<<endl;
    }
};
class child2:public parent{
    public:
    string name="hi";
    void dis(){
        cout<<"subchild class"<<endl;
    }
};
class subchild:public child1,public child2{
    public:
    void play(){
        cout<<"hybrid"<<endl;
    }
};
int main(){
    subchild ch;
    cout<<ch.name<<endl<<ch.f<<endl;
    ch.dis();
    //ch.display();   //it shows an ambiguous error
    ch.child1::display();
    ch.disp();
    ch.play();
    return 0;
}