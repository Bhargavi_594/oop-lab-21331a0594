public class ArithmeticExp{
    public static void main(String[] args){
        try{
            System.out.println(10/0);
        }catch(ArithmeticException ex){
            System.out.println("Denomenator must be a nonzero value.\nOtherwise it shows \n"+ex);
        }
    }
}