interface s1{
    public default void dis(){
        System.out.println("Interface s1");
    }
}
interface s2{
    public default void dis2(){
        System.out.print("Interface s2\n");
    }
}
class sub implements s1,s2{
    public void display(){
        s1.super.dis();
        s2.super.dis2();
    }
}
public class multipleinher {
    public static void main(String []args){
        sub s=new sub();
        s.display();
    }
}
