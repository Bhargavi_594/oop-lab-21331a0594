class parent{
    int p=10;
}
//single inheritance    ,   parent <- child1
class child1 extends parent{
    int ch1=20;
}
//multilevel inheritance    ,   parent <-child1 <- subchild
class subchild extends child1{
    int sc=30;
}
class child2 extends parent{
    int ch=2;
}
public class inheritance {
    public static void main(String[] args){
        child1 c1=new child1();
        System.out.println("single inheritance : "+(c1.ch1+c1.p));
        subchild s=new subchild();
        System.out.println("multilevel inheritance : "+(s.sc+s.ch1+s.p));
        child2 c2=new child2();
        System.out.println("hierarchical inheritance : "+(c2.ch+c2.p));
    }
}
