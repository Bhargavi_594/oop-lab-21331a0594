public class Immutable{
    public static void main(String[] args){
        int a=10;
        System.out.println("Int a before address : "+System.identityHashCode(a));
        a=20;
        System.out.println("Int a after address : "+System.identityHashCode(a));

        String s="java";
        System.out.println("String before address : "+System.identityHashCode(s));
        s=s.replace("java", "c++");
        System.out.println("String after address : "+System.identityHashCode(s));

        StringBuffer c=new StringBuffer("Hello");
        System.out.println("StringBuffer before address : "+System.identityHashCode(c));
        c=c.replace(0,5,"Hii");
        System.out.println("StringBuffer address : "+System.identityHashCode(c));
    }
}