public class AngularMath {
    public static void main(String[] args) {
        System.out.println("Radians angle to equivalent angle in Degrees : "+Math.toDegrees(2*Math.PI));
        System.out.println("Degrees angle to equivalent angle in Radians: "+Math.toRadians(180));
    }
}
