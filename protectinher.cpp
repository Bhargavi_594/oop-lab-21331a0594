#include<iostream>
using namespace std;
class parent{
    public:
    int n1=5;
};
class child1:protected parent{
    public:
        void dispaly(){
            cout<<"protected value : "<<n1<<endl;
        }

};
class child2:public child1{
    public:
        void display(){
            cout<<"accessed from parent class : "<<n1<<endl;
        }
};
int main(){
    child1 ch1;
    //cout<<"protected inheritance : "<<ch1.n1;  // it shows an error because protected class values accessed only in derived classes
    ch1.dispaly();
    child2 ch2;
    ch2.display();
    return 0;
}