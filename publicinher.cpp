#include<iostream>
using namespace std;
class parent{
    public:
    int n1=5;
};
class child1:public parent{
    public:
        void dispaly(){
            cout<<"protected value : "<<n1<<endl;
        }

};
class child2:public parent{
    public:
        void display(){
            cout<<"accessed from parent class : "<<n1<<endl;
        }
};
int main(){
    child1 ch1;
    cout<<"public inheritance : "<<ch1.n1<<endl;
    ch1.dispaly();
    child2 ch2;
    cout<<"public inheritance : "<<ch2.n1<<endl;
    ch2.display();
    return 0;
}