class Student{
    String collegeName;
    int collegeCode;
    String fullName;
    double semPerentage;
    //default constructor
    Student(){
        collegeName="MVGR";
        collegeCode=33;
    }
    //parameterized constructor
    Student(String fn,double sp){
        fullName=fn;
        semPerentage=sp;
    }
    void display1(){
        System.out.println("parameterized constructor : "+fullName+" "+semPerentage);
    }
    void display(){
        System.out.println("default constructor : "+collegeName+" "+collegeCode);
    }
    //destructor
    protected void finalize(){
        System.out.println("dead");
    }
}
public class constructor2 {
    public static void main(String[] args){
        Student s=new Student();
        Student s1=new Student("Ravi",69);
        s.display();
        s1.display1();
        s=s1=null;
        System.gc();
    }
}
