import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
class Account{
    Frame Acct_f;
    static Label billing_t;
    static String cus_address;
    Account(){
        Acct_f=new Frame();
        Label Id=new Label("Id ");
        Id.setBounds(100,100,80,20);
        Label id_t=new Label(webuser.Login_T.getText());
        id_t.setBounds(200,100,100,20);

        Label billing_address=new Label("Billing Address ");
        billing_address.setBounds(100,130,80,20);
        billing_t=new Label(cus_address=customer.address_t.getText());
        billing_t.setBounds(200,130,80,20);

        Label isclosed=new Label("Is_closed");
        isclosed.setBounds(100,160,80,20);
        CheckboxGroup ck=new CheckboxGroup();
        Checkbox c1=new Checkbox("Yes",ck,false);
        c1.setBounds(200,160,50,20);
        Checkbox c2=new Checkbox("No",ck,true);
        c2.setBounds(260,160,50,20);
        Acct_f.add(c1);Acct_f.add(c2);

        Label open=new Label("Opened date ");
        open.setBounds(100,190,80,20);
        TextField open_t=new TextField();
        open_t.setBounds(200,190,80,20);

        Label closed=new Label("Closed date ");
        closed.setBounds(100,220,80,20);
        TextField closed_t=new TextField();
        closed_t.setBounds(200,220,80,20);

        Button back=new Button("Back");
        back.setBounds(150,250,80,20);
        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                if(open_t.getText().isEmpty() || closed_t.getText().isEmpty()){
                    JOptionPane.showMessageDialog(Acct_f,"All fields must be filled.","Error",JOptionPane.ERROR_MESSAGE);
                }
                else{
                    Acct_f.dispose();
                }
            }
        });

        Acct_f.add(Id);Acct_f.add(id_t);Acct_f.add(billing_address);Acct_f.add(billing_t);Acct_f.add(back);
        Acct_f.add(isclosed);Acct_f.add(open);Acct_f.add(open_t);Acct_f.add(closed);Acct_f.add(closed_t);

        
        Acct_f.setTitle("Account");
        Acct_f.setSize(500, 500);
        Acct_f.setLayout(null);
        Acct_f.setVisible(true);

        Acct_f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                Acct_f.dispose();
            }
        });
    }
}
 
class Product extends Frame {
    Product(){
        Label id=new Label("Product id ");
        Label name=new Label("Name ");
        Label Supplier=new Label("Supplier ");

        Label id_t=new Label(ShoppingCart.id);
        Label name_t=new Label(ShoppingCart.laptop.getText());
        Label Supplier_t=new Label("Supplier ");

        id.setBounds(100,100,80,20);
        name.setBounds(100,130,80,20);
        Supplier.setBounds(100,160,80,20);
        id_t.setBounds(200,100,100,20);
        name_t.setBounds(200,130,100,20);
        Supplier_t.setBounds(200,160,100,20);

        Button order=new Button("Order");
        order.setBounds(200,200,80,20);
        order.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                dispose();
                new Order();
            }
        });

        Button back=new Button("Back");
        back.setBounds(100,200,80,20);
        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                dispose();
                new ShoppingCart();
            }
        });

        add(id);add(id_t);add(name);add(name_t);add(back);
        add(Supplier);add(Supplier_t);add(order);

        setTitle("Product");
        setSize(500, 500);
        setLayout(null);
        setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                dispose();
            }
        });
    } 
}

class Payment{
    Frame f;
    Payment(){
        f=new Frame();
        Label id=new Label("Id");
        Label totolamt=new Label("Total price");

        Label id_l=new Label(ShoppingCart.id);
        Label totolamt_l=new Label(ShoppingCart.totalprice);

        id.setBounds(100,100,80,20);
        id_l.setBounds(200,100,80,20);
        totolamt.setBounds(100,130,80,20);
        totolamt_l.setBounds(200,130,80,20);

        Label p=new Label("Payment option ");
        p.setBounds(100,160,100,20);

        CheckboxGroup cg=new CheckboxGroup();
        Checkbox c1=new Checkbox("Paytm UPI", cg, true);
        c1.setBounds(120,190,100,20);
        Checkbox c2=new Checkbox("Phonepe UPI", cg, false);        
        c2.setBounds(120,220,100,20);
        Checkbox c3=new Checkbox("Credit/Debit/ATM", cg, false);
        c3.setBounds(120,250,100,20);
        Checkbox c4=new Checkbox("Net Banking", cg, false);
        c4.setBounds(120,280,100,20);
        Checkbox c5=new Checkbox("Cash on Delivery", cg, false);
        c5.setBounds(120,310,100,20);

        Button cont=new Button("Continue");
        cont.setBounds(130,350,80,20);
        cont.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                JOptionPane.showMessageDialog(f,"Order conformed \n Thank you for ordering");
                int a=JOptionPane.showConfirmDialog(f,"Continue shopping...");
                if(a==JOptionPane.YES_OPTION){
                    f.dispose();
                    new ShoppingCart();
                }
                else if(a==JOptionPane.NO_OPTION){
                    f.dispose();
                }
            }
        });

        f.add(id);f.add(id_l);f.add(totolamt);f.add(totolamt_l);f.add(p);f.add(c1);f.add(c2);f.add(c3);f.add(c4);f.add(c5);f.add(cont);


        f.setTitle("Payment");
        f.setBackground(Color.lightGray);
        f.setSize(500, 500);
        f.setLayout(null);
        f.setVisible(true);

        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                f.dispose();
            }
        });
    }
}

class Order extends Frame{
    Order(){
        Label number=new Label("number");
        Label number_l=new Label(ShoppingCart.id);
        number.setBounds(100,100,80,20);
        number_l.setBounds(200,100,80,20);
    
        Label orderdate=new Label("Ordered Date");
        Label orderdate_l=new Label("24/10/2023");
        orderdate.setBounds(100,130,80,20);
        orderdate_l.setBounds(200,130,80,20);

        Label shippeddate=new Label("Shipped date");
        Label shippeddate_l=new Label("27/10/2023");
        shippeddate.setBounds(100,160,80,20);
        shippeddate_l.setBounds(200,160,80,20);

        Label status=new Label("Status");
        Label status_l=new Label("Shipped");
        status.setBounds(100,190,80,20);
        status_l.setBounds(200,190,80,20);

        Label total=new Label("Total");
        Label total_l=new Label(ShoppingCart.totalprice);
        total.setBounds(100,210,80,20);
        total_l.setBounds(200,210,80,20);

        Label ship_to=new Label("Shipped address");
        Label ship_to_l=new Label(Account.cus_address);
        ship_to.setBounds(100,240,100,20);
        ship_to_l.setBounds(210,240,200,20);

        Button payment=new Button("Payment");
        payment.setBounds(150,270,80,20);
        payment.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                dispose();
                new Payment();
            }
        });

        add(number);add(orderdate);add(orderdate_l);add(shippeddate);add(shippeddate_l);add(ship_to);
        add(status);add(status_l);add(total);add(total_l);add(number_l);add(ship_to_l);add(payment);
        
        setTitle("Order");
        setSize(500, 500);
        setLayout(null);
        setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                dispose();
            }
        });
    }
}

class LineItem extends Frame{
    static Label quantity;
    static  Label price,product_id;
    static Label t1,t2,total,total_p;
    LineItem(){
        quantity=new Label("Quantity");
        price=new Label("Price");
        t1=new Label();
        t2=new Label();

        Label l=new Label("Product ID ");
        l.setBounds(100,70,80,20);
        product_id=new Label();
        product_id.setBounds(200,70,80,20);
        add(product_id);add(l);

        quantity.setBounds(100,100,80,20);
        price.setBounds(100,130,80,20);
        t1.setBounds(200,100,80,20);
        t2.setBounds(200,130,80,20);

        total =new Label("Total price");
        total.setBounds(100,160,80,20);
        
        total_p =new Label();
        total_p.setBounds(200,160,80,20); 

        Button back=new Button("Back");
        back.setBounds(100,210,80,20);
        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                dispose();
                new ShoppingCart();
            }
        });
        Button next=new Button("Next");
        next.setBounds(200,210,80,20); 
        next.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                dispose();
                new Product();
            }
        });  
        
        add(back);add(next);add(total);add(total_p);
        add(quantity);add(price);add(t1);add(t2);
        setTitle("LineItem");
        setSize(500, 500);
        setLayout(null);
        setVisible(true);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                dispose();
            }
        });
    }
}

class ShoppingCart extends Frame implements ActionListener{
    static Label laptop,frock,shirt,p1,p2,p3;
    static JButton lap_b1,fro_b2,shirt_b3;
    static Label label1,label2,label3;
    static String id,totalprice;
    ShoppingCart(){
        lap_b1=new JButton(new ImageIcon("lap.png"));
        laptop=new Label("Laptop");
        laptop.setBounds(170,100,80,20);
        p1=new Label("Rs. 47009");
        p1.setBounds(170,115,80,20);
        Button minu1=new Button("-");
        minu1.setBounds(170,140,15,15);
        label1=new Label("1");
        label1.setBounds(190,140,15,15);
        Button plus1=new Button("+");
        plus1.setBounds(205,140,15,15);
        Label Q1=new Label(" Quantity");
        Q1.setBounds(225,140,50,20);
        minu1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String s1=label1.getText();
                if(Integer.parseInt(s1)!=0)
                label1.setText(String.valueOf(Integer.parseInt(s1)-1));
            }
        });
        plus1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String s1=label1.getText();
                if(Integer.parseInt(s1)!=0)
                label1.setText(String.valueOf(Integer.parseInt(s1)+1));
            }
        });

        fro_b2=new JButton(new ImageIcon("kid.png"));
        frock=new Label("Frock");
        frock.setBounds(170,170,80,20);
        p2=new Label("Rs. 599");
        p2.setBounds(170,190,80,20);
        Button minu2=new Button("-");
        minu2.setBounds(170,210,15,15);
        label2=new Label("1");
        label2.setBounds(190,210,15,15);
        Button plus2=new Button("+");
        plus2.setBounds(205,210,15,15);
        Label Q2=new Label(" Quantity");
        Q2.setBounds(225,210,50,20);
        minu2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String s1=label2.getText();
                if(Integer.parseInt(s1)!=0)
                label2.setText(String.valueOf(Integer.parseInt(s1)-1));
            }
        });
        plus2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String s1=label2.getText();
                label2.setText(String.valueOf(Integer.parseInt(s1)+1));
            }
        });

        shirt_b3=new JButton(new ImageIcon("shirt.png"));
        shirt=new Label("Shirt");
        shirt.setBounds(170,240,80,20);
        p3=new Label("Rs. 799");
        p3.setBounds(170,260,80,20);
        Button minu3=new Button("-");
        minu3.setBounds(170,280,15,15);
        label3=new Label("1");
        label3.setBounds(190,280,15,15);
        Button plus3=new Button("+");
        plus3.setBounds(205,280,15,15);
        Label Q3=new Label(" Quantity");
        Q3.setBounds(225,280,50,20);
        minu3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String s1=label3.getText();
                if(Integer.parseInt(s1)!=0)
                label3.setText(String.valueOf(Integer.parseInt(s1)-1));
            }
        });
        plus3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String s1=label3.getText();
                label3.setText(String.valueOf(Integer.parseInt(s1)+1));
            }
        });

        lap_b1.setBounds(100,100,60,60);
        fro_b2.setBounds(100,170,60,60);
        shirt_b3.setBounds(100,240,60,60);

        lap_b1.addActionListener(this);
        fro_b2.addActionListener(this);
        shirt_b3.addActionListener(this);

        Button account=new Button("Account");
        account.setBounds(100,210,80,20);
        account.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                new Account();
            }
        });
        account.setBounds(100,310,80,20);

        add(shirt_b3);add(fro_b2);add(lap_b1);add(laptop);add(frock);add(shirt);
        add(p1);add(p2);add(p3);add(plus1);add(label1);add(minu1);add(plus2);add(label2);add(minu2);
        add(plus3);add(label3);add(minu3);add(Q2);add(Q1);add(Q3);add(account);
        setTitle("ShoppingCart");
        setSize(500, 500);
        setLayout(null);
        setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                dispose();
            }
        });
    }
    public void actionPerformed(ActionEvent e){
        dispose();
        new LineItem();
        if(e.getSource()==lap_b1){
            LineItem.t1.setText(label1.getText());
            LineItem.t2.setText(p1.getText());
            LineItem.product_id.setText(id="84823975");
            LineItem.total_p.setText(totalprice=String.valueOf(Integer.parseInt(label1.getText())*Integer.parseInt(p1.getText().substring(4))));

        }
        else if(e.getSource()==fro_b2){
            LineItem.t1.setText(label2.getText());  
            LineItem.t2.setText(p2.getText());
            LineItem.product_id.setText(id="345809234");
            LineItem.total_p.setText(totalprice=String.valueOf(Integer.parseInt(label2.getText())*Integer.parseInt(p2.getText().substring(4))));
        }
        else if(e.getSource()==shirt_b3){
            LineItem.t1.setText(label3.getText());
            LineItem.t2.setText(p3.getText());
            LineItem.product_id.setText(id="63492849");
            LineItem.total_p.setText(totalprice=String.valueOf(Integer.parseInt(label3.getText())*Integer.parseInt(p3.getText().substring(4))));
        }
    }
}

class customer{
    static Frame customer_f;
    static TextArea address_t;
    customer(){
        customer_f=new Frame();
        Label Id=new Label("Id ");
        Label Address=new Label("Address ");
        Label Phone=new Label("Phone ");
        Label Email=new Label("Email ");

        Label id_t=new Label(webuser.Login_T.getText());
        address_t=new TextArea();
        TextField phone_t=new TextField();
        TextField email_t=new TextField();

        Id.setBounds(100,100,80,20);
        Address.setBounds(100,150,80,20);
        Phone.setBounds(100,220,80,20);
        id_t.setBounds(200,100,100,20);
        address_t.setBounds(200,150,100,50);
        phone_t.setBounds(200,220,100,20);
        Email.setBounds(100,250,80,20);
        email_t.setBounds(200,250,100,20);

        Button close=new Button("Submit & Close");
        close.setBounds(150,290,80,20);
        close.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                if(address_t.getText().isEmpty() || phone_t.getText().isEmpty() || email_t.getText().isEmpty()){
                    JOptionPane.showMessageDialog(customer_f,"All fields must be filled.","Empty",JOptionPane.ERROR_MESSAGE);
                }
                else if (phone_t.getText().length()!=10){
                    JOptionPane.showMessageDialog(customer_f,"Phone number must be 10 digits","Empty",JOptionPane.WARNING_MESSAGE);
                }
                else{
                    customer_f.dispose();
                }
            }
        });

        customer_f.add(id_t);customer_f.add(Id);customer_f.add(Address);customer_f.add(address_t);customer_f.add(close);
        customer_f.add(Phone);customer_f.add(phone_t);customer_f.add(Email);customer_f.add(email_t);

        customer_f.setTitle("Customer");
        customer_f.setSize(500, 500);
        customer_f.setLayout(null);
        customer_f.setVisible(true);

        customer_f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                customer_f.dispose();
            }
        });
    }
}

class webuser{
    public static Frame f;
    static Label Login_id,password;
    static TextField Login_T;
    static JPasswordField pass_T;
    webuser(){
        f=new Frame();
        Login_id=new Label("Login_id ");
        password=new Label("Password ");

        Login_T=new TextField();
        pass_T=new JPasswordField();

        Button submit=new Button("Submit");
        submit.setBounds(150,210,80,20);
        submit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                String str=Login_T.getText();
                String str1=pass_T.getText();
                if(str.isEmpty() || str1.isEmpty()){
                    JOptionPane.showMessageDialog(f,"Login id and password must be filled.","Empty",JOptionPane.ERROR_MESSAGE);
                }
                else{
                    if(str.equals("Ravi") && str1.equals("R@123")){
                        JOptionPane.showMessageDialog(f,"This account Active now\n Proceed","Userstatus",JOptionPane.DEFAULT_OPTION);
                        new customer();
                    }
                    else if(str.equals("Josh") && str1.equals("Josh123")){
                        JOptionPane.showMessageDialog(f,"This account Blocked","Userstatus",JOptionPane.ERROR_MESSAGE);
                       f. dispose();
                    }
                    else{
                        JOptionPane.showMessageDialog(f,"New Account created","Userstatus",JOptionPane.DEFAULT_OPTION);
                        new customer();
                    }
                    Button shopping=new Button("ShoppingCart");
                    shopping.setBounds(150,250,80,20);
                    shopping.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e){
                            f.dispose();
                            new ShoppingCart();
                        }
                    });
                    f.add(shopping);
                }
            }
        });

        Login_id.setBounds(100,100,80,20);
        password.setBounds(100,150,80,20);
        Login_T.setBounds(200,100,100,20);
        pass_T.setBounds(200,150,100,20);

        f.add(Login_id);f.add(password);f.add(Login_T);
        f.add(pass_T);f.add(submit);

        f.setTitle("Webuser");
        f.setSize(500, 500);
        f.setLayout(null);
        f.setVisible(true);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                f.dispose();
            }
        });
    }
    
    public static void main(String[] args){
        new webuser();
    }
}