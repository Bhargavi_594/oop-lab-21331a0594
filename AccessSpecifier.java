class AccessSpecifierDemo{
    private int priVar;
    protected int proVar;
    public int pubVar;
    public void setVar(int priValue,int proValue, int pubValue){
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    public void getVar(){
        System.out.println("Member variables : "+" "+priVar+" "+proVar+" "+pubVar);
    }
}
public class AccessSpecifier {
    public static void main(String[] args){
        AccessSpecifierDemo a=new AccessSpecifierDemo();
        a.setVar(1,2,3);
        a.getVar();    
    }
}
