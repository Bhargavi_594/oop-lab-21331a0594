#include<iostream>
using namespace std;
class parent{
    protected:
        int d=5;
    public:
        int a=10;
};
class parent2{
    public:
        int p2=4;
};
//single inheritance    ,   parent <- single
class single:public parent{
    public:
        int b=20;
};
//multilevel    ,   parent <- single <- multilevel
class multilevel:public single{
    public:
        int c=30;
        void display(){
            cout<<c<<ends<<d<<endl;
        }
};
//hierarchical  ,   parent <- single  &&  parent <- child
class child:public parent{
    public:
        int ch2=3;
};
//multiple  ,   parent <-multiple  &&  parent2 <- multiple
class multiple:public parent2,public parent{
    public:
        int m=7;
};
//hybrid    ,   hierarchical and multiple
class hybrid:public single,public child{
    public:
        int h=7;
};
int main(){
    single s;
    cout<<"single inheritance : "<<s.a+s.b<<endl;
    child ch1;
    cout<<"hierarchical inheritance : "<<s.a+ch1.a<<endl;
    multilevel s1;
    cout<<"multilevel inheritance : "<<s1.a+s1.b+s1.c<<endl;
    multiple m1;
    cout<<"multiple inheritance : "<<m1.a+m1.p2+m1.m<<endl;
    hybrid h1;
    cout<<"hybrid inheritance : "<<h1.h+h1.ch2+h1.h+h1.b<<endl;
    //cout<<"hybrid inheritance : "<<h1.a<<endl;  //it shows an ambiguous error
    return 0;
}