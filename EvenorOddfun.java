import java.util.*;
public class EvenorOddfun {
    static void evenorodd1(int n){
        if(n%2==0){
            System.out.print("Given number is even.");
        }
        else{
            System.out.print("Given number is odd.");
        }
    }
    public static void main(String []args){
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter a number : ");
        int n=sc.nextInt();
        evenorodd1(n);
        sc.close();
    }
}
