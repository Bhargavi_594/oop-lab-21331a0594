#include<iostream>
#include <iomanip>
using namespace std;
int main(){
    cout<<"Hello world";
    cout<<endl<<"Hello "<<endl<<"world"<<endl;
    cout<<"Hello\nworld\n"<<endl;

    cout<<setw(10)<<"Hello"<<endl;
    cout<<setfill('x')<<setw(10)<<"Hello"<<endl;

    cout<<"Hello"<<ends<<"world"<<endl;

    double d=23.19678490303;
    cout<<setprecision(5)<<d<<endl;
    cout<<setprecision(10)<<d<<endl;
    return 0;
}