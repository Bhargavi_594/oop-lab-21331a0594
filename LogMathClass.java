import java.lang.Math;
public class LogMathClass {    
    public static void main(String[] args)     
    {    
        double x = 70;    
        double y = 12;    
        System.out.println("Logarithm of x is: " + Math.log(x));   
        System.out.println("Logarithm of y is: " + Math.log(y));    
        System.out.println("log10 of x is: " + Math.log10(x));   
        System.out.println("log10 of y is: " + Math.log10(y));
        System.out.println("log1p of x is: " +Math.log1p(x));
        System.out.println("exp of a is: " +Math.exp(x)); 
        System.out.println("expm1 of a is: " +Math.expm1(x));  
    }    
}    