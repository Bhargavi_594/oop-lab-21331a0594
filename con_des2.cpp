#include<iostream>
using namespace std;
class Student{
    public:
    string fullname;
    double semPercentage;
    string collegeName;
    int collegeCode;
    //default constructor
    Student(){
        collegeName="MVGR";
        collegeCode=33;
    }
     void display(){
        cout<<"collegeName : "<<collegeName<<"\ncollegeCode: "<<collegeCode<<endl;
    }
    //parameterized constructor
    Student(string name,double s){
        fullname=name;
        semPercentage=s;
    }
    void display1(){
        cout<<"fullname : "<<fullname<<"\nsemPercentage : "<<semPercentage<<endl;
    }
   
};
int main(){
    Student s;
    Student s1=Student("Joshi",90);
    s.display();
    s1.display1();
    return 0;
}