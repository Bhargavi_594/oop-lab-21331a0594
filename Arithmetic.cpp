#include<iostream>
using namespace std;
class Arithmetic{
    public:
        float a,b;
        char ch;
        Arithmetic(float a1,float b1,char ch1){
            a=a1;
            b=b1;
            ch=ch1;
        }
    public:
        void Operation(){
            switch(ch){
            case '+': cout<<"Addition : "<<(a+b)<<endl;
            break;
            case '-': cout<<"Subtraction : "<<(a-b)<<endl;
            break;
            case '*': cout<<"Multiplication : "<<(a*b)<<endl;
            break;
            case '/': cout<<"Division : "<<(a/b)<<endl;
            break;
            case '%': cout<<"Modulo : "<<(int(a)%int(b))<<endl;
            break;
            default: cout<<"Invalid operator";
            break;
        }
    }
};
int main(){
    char ch;
    float a,b;
    cout<<"Enter the a and b values : ";
    cin>>a>>b;
    cout<<"Enter the operator : ";
    cin>>ch;
    Arithmetic a1=Arithmetic(a,b,ch);
    a1.Operation();
    return 0;
}