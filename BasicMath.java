import java.lang.Math;
public class BasicMath {
    public static void main(String[] args) {
        int a=10;
        int b=20;
        System.out.println("Absolute value : "+Math.abs(-12));
        System.out.println("Minimum value : "+Math.min(a,b));
        System.out.println("Maximum value : "+Math.max(a,b));
        System.out.println("Round value : "+Math.round(12.5356));
        System.out.println("Square root : "+Math.sqrt(64));
        System.out.println("Cube root : "+Math.cbrt(27));
        System.out.println("Power : "+Math.pow(2,3));
        System.out.println("Floor value for negative numbers : "+Math.floor(-14.234));
        System.out.println("Ceil value for negative numbers : "+Math.ceil(-14.234));
        System.out.println("Floor value for positive numbers : "+Math.floor(14.234));
        System.out.println("Ceil value for positive numbers : "+Math.ceil(14.234));
        System.out.println("Round method : "+Math.round(50.92345666));

    }
}
