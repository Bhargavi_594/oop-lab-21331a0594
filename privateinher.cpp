#include<iostream>
using namespace std;
class parent{
    public:
    int n1=5;
};
class child1:private parent{
    public:
    int n=n1;
        void dispaly(){
            cout<<"protected value : "<<n1<<endl;
        }

};
class child2:public child1{
    public:
        void display(){
            //cout<<"private class values  : "<<n1<<endl;  // it shows an error because base class is private to child1
           // cout<<"from child1 class : "<<n<<endl;
           cout<<"private class values can be accessed within the class only\n";
        }
};
int main(){
    child1 ch1;
    ch1.dispaly();
    child2 ch2;
    ch2.display();
    return 0;
}