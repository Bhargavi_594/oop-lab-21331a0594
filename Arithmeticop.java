import java.util.*;
public class Arithmeticop {
    static public void main(String args[]){
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter the a and b values : ");
        float a=sc.nextFloat();
        float b=sc.nextFloat();
        System.out.print("Enter the operator : ");
        char op=sc.next().charAt(0);
        if(op=='+'){
            System.out.println("Addition : "+(a+b));
        }
        else if(op=='-'){
            System.out.println("subtraction : "+(a-b));
        }
        else if(op=='*'){
            System.out.println("Multiplication : "+(a*b));
        }
        else if(op=='/'){
            System.out.println("Division : "+(a/b));
        }
        else if(op=='%'){
            System.out.println("modulo : "+(a%b));
        }
        else {
            System.out.println("Invalid operator");
        }
        sc.close();
    }
}
