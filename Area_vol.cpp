#include<iostream>
using namespace std;
class box{
    public:
    float a;
    float x,y,z;
    void insert(float l,float b,float h){
        x=l;
        y=b;
        z=h;
    }
    void boxArea(float length,float width,float height){
        a=2*(length*width+width*height+length*height);
        cout<<"The area of box : "<<a<<endl;
    }
    void boxVolume(float length,float width,float height);
    friend void displayBoxDimension(box);
    inline void displayWelcomeMessage();
};
void displayBoxDimension(box A){
    cout<<"length: "<<A.x<<endl<<"width : "<<A.y<<endl<<"height : "<<A.z<<endl;
}
float v;
void box:: boxVolume(float length,float width,float height){
    v=length*width*height;
    cout<<"The volume of box : "<<v<<endl;
}
void box:: displayWelcomeMessage(){
    cout<<"Welcome inline function"<<endl;
}
int main(){
    float l,b,h;
    cout<<"The values l,b and h : ";
    cin>>l>>b>>h;
    box A;
    A.insert(l,b,h);
    A.boxArea(l,b,h);
    A.boxVolume(l,b,h);
    displayBoxDimension(A);
    A.displayWelcomeMessage();
    return 0;
}