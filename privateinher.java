class parent{
    private int a=10;
    void display(){
        System.out.println(a);
    }
}
class child1 extends parent{
    int b=20;
    void dis(){
        //System.out.println(a);
    }
}
public class privateinher {
    public static void main(String[] args){
        child1 s=new child1();
        //System.out.println(s.a);  //it shows an error because private variables accessed within class only
        System.out.println(s.b);
        s.display();
        s.dis();
    }
}