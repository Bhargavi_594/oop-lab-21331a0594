#include<iostream>
using namespace std;
class Student{
    public:
    string fullname;
    int rollNum;
    double semPercentage;
    string collegeName;
    int collegeCode;
    //constructor
    Student(string name,int r,double s,string cName,int Code){
        fullname=name;
        rollNum=r;
        semPercentage=s;
        collegeName=cName;
        collegeCode=Code;
    }
    void display(){
        cout<<"fullname : "<<fullname<<"\nrollNum : "<<rollNum<<"\nsemPercentage : "<<semPercentage<<"\ncollegeName : "<<collegeName<<"\ncollegeCode: "<<collegeCode<<endl;
    }
    //destructor
    ~Student(){
        cout<<"destructor invoke\n";
    }
};
int main(){
    Student s1=Student("Joshi",12,90,"MVGR",5324);
    s1.display();
    return 0;
}