class Student{
    static String collegeName;
    static int collegeCode;
    static String fullName;
    static double semPerentage;
    //default constructor
    Student(){
        collegeName="MVGR";
        collegeCode=33;
    }
    //parameterized constructor
    Student(String fn,double sp){
        fullName=fn;
        semPerentage=sp;
    }
    void display(){
        System.out.println("Student details : "+collegeName+" "+collegeCode+" "+fullName+" "+semPerentage);
    }
    //destructor
    protected void finalize(){
        System.out.println("dead");
    }
}
public class cons2 {
    public static void main(String[] args){
        Student s=new Student();
        Student s1=new Student("sai",12);
        s.display();
        s=s1=null;
        System.gc();
    }
}